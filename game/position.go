package game

// Position Represents a position in 2D space.
type Position struct {
	x float64
	y float64
}

// X returns the x value for position.
func (position *Position) X() float64 {
	return position.x
}

// Y returns the y value for position.
func (position *Position) Y() float64 {
	return position.y
}

// SetX sets the x value for the position.
func (position *Position) SetX(value float64) {
	position.x = value
}

// SetY sets the y value for the position.
func (position *Position) SetY(value float64) {
	position.y = value
}

// Add takes the given pointer object and adds it to the position object.
func (position *Position) Add(pointer Pointer) {
	position.x += pointer.X()
	position.y += pointer.Y()
}

// Subtract takes the given pointer object and subtracts it from the position object.
func (position *Position) Subtract(pointer Pointer) {
	position.x -= pointer.X()
	position.y -= pointer.Y()
}

// Multiply takes the given value and multiplies the position by it.
func (position *Position) Multiply(value float64) {
	position.x *= value
	position.y *= value
}

// Divide takes the given value and divides the position by it.
func (position *Position) Divide(value float64) {
	position.x /= value
	position.y /= value
}

// Copy copies a pointer object into position.
func (position *Position) Copy(pointer Pointer) {
	position.x = pointer.X()
	position.y = pointer.Y()
}

// NewPosition creates a new Position object.
func NewPosition(x, y float64) (position *Position) {
	position = new(Position)
	position.x = x
	position.y = y
	return position
}

// NewPositionCopy creates a new Position object from another Pointer object.
func NewPositionCopy(pointer Pointer) (position *Position) {
	position = NewPosition(pointer.X(), pointer.Y())
	return position
}
