package game

import (
	"time"
)

// TimeStep is an object that contains data for time stepping (framerate control).
type TimeStep struct {
	time        float64
	deltaTime   float64
	currentTime float64
	accumulator float64
}

// NewTimestep creates a new TimeStep with the given delta time.
func NewTimestep(deltaTime float64) (timeStep *TimeStep) {
	timeStep = new(TimeStep)
	timeStep.deltaTime = deltaTime
	timeStep.currentTime = float64(time.Now().UnixNano()) / 1000000.0
	return timeStep
}

// Update will update the timestep information.
func (timeStep *TimeStep) Update() {
	newTime := float64(time.Now().UnixNano()) / 1000000.0
	frameTime := newTime - timeStep.currentTime
	timeStep.currentTime = newTime
	timeStep.accumulator += frameTime
}

// Accumulate will accumulate the timestep and run the given integral function.
func (timeStep *TimeStep) Accumulate(function func(float64)) {
	for timeStep.accumulator >= timeStep.deltaTime {
		function(timeStep.deltaTime)
		timeStep.accumulator -= timeStep.deltaTime
		timeStep.time += timeStep.deltaTime
	}
}
