package game

// Pointer represents a 2D point object.
type Pointer interface {
	X() float64
	Y() float64
	SetX(float64)
	SetY(float64)
	Add(Pointer)
	Subtract(Pointer)
	Multiply(float64)
	Divide(float64)
	Copy(Pointer)
}
