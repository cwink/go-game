package game

// Velocity represents a velocity vector.
type Velocity struct {
	magnitude float64
	position  *Position
}

// X returns the x value for velocity.
func (velocity *Velocity) X() float64 {
	return velocity.position.x
}

// Y returns the y value for velocity.
func (velocity *Velocity) Y() float64 {
	return velocity.position.y
}

// SetX sets the x value for the velocity.
func (velocity *Velocity) SetX(value float64) {
	velocity.position.x = value
}

// SetY sets the y value for the velocity.
func (velocity *Velocity) SetY(value float64) {
	velocity.position.y = value
}

// Add takes the given pointer object and adds it to the velocity object.
func (velocity *Velocity) Add(pointer Pointer) {
	velocity.position.x += pointer.X()
	velocity.position.y += pointer.Y()
}

// Subtract takes the given pointer object and subtracts it from the velocity object.
func (velocity *Velocity) Subtract(pointer Pointer) {
	velocity.position.x -= pointer.X()
	velocity.position.y -= pointer.Y()
}

// Multiply takes the given value and multiplies the velocity by it.
func (velocity *Velocity) Multiply(value float64) {
	velocity.position.x *= value
	velocity.position.y *= value
}

// Divide takes the given value and divides the velocity by it.
func (velocity *Velocity) Divide(value float64) {
	velocity.position.x /= value
	velocity.position.y /= value
}

// Copy copies a pointer object into position.
func (velocity *Velocity) Copy(pointer Pointer) {
	velocity.position.x = pointer.X()
	velocity.position.y = pointer.Y()
}

// CopyVelocity copies a given velocity object into the velocity object.
func (velocity *Velocity) CopyVelocity(copy *Velocity) {
	velocity.position.x = copy.X()
	velocity.position.y = copy.Y()
	velocity.magnitude = copy.Magnitude()
}

// Magnitude retrieves the magnitude of the velocity vector.
func (velocity *Velocity) Magnitude() float64 {
	return velocity.magnitude
}

// SetMagnitude sets the magnitude of the velocity vector.
func (velocity *Velocity) SetMagnitude(value float64) {
	velocity.magnitude = value
}

// NewVelocity creates a new Velocity object.
func NewVelocity(x, y, magnitude float64) (velocity *Velocity) {
	velocity = new(Velocity)
	velocity.position = NewPosition(x, y)
	velocity.magnitude = magnitude
	return velocity
}

// NewVelocityCopy creates a new Velocity object from another Velocity object.
func NewVelocityCopy(copy Velocity) (velocity *Velocity) {
	velocity = NewVelocity(copy.position.X(), copy.position.Y(), copy.magnitude)
	return velocity
}
