package game

// Color represents a color structure.
type Color struct {
	Red   uint8
	Green uint8
	Blue  uint8
	Alpha uint8
}

// Copy copies a color object into another color object.
func (color *Color) Copy(copy *Color) {
	color.Red = copy.Red
	color.Green = copy.Green
	color.Blue = copy.Blue
	color.Alpha = copy.Alpha
}

// NewColor creates a new color object.
func NewColor(red, green, blue, alpha uint8) (color *Color) {
	color = new(Color)
	color.Red = red
	color.Blue = blue
	color.Green = green
	color.Alpha = alpha
	return color
}

// NewColorCopy creates a new Color object from another Pointer object.
func NewColorCopy(pointer Pointer) (color *Color) {
	color = NewColor(color.Red, color.Green, color.Blue, color.Alpha)
	return color
}
